package control;

import model.ContaCorrente;

public class Fornecedor extends Thread {

	private ContaCorrente conta;
	
	public Fornecedor (ContaCorrente conta){
		this.conta = conta;
	}

	@Override
	public void run() {
		int contador = 100;
		
		try{
			
			for (int i = 0; i < contador; i++) {
				conta.sacar(100);
			}	
			
		} catch(IllegalArgumentException e){
			
			System.out.println("Saldo Insuficiente para sacar 100 reais pois o saldo no momento é de " + conta.getSaldo() + " reais");
			
		}		
		
		System.out.println("Fim dos saques");
		
	}
	
}
