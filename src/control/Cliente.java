package control;

import model.ContaCorrente;

public class Cliente extends Thread {

private ContaCorrente conta;
	
	public Cliente (ContaCorrente conta){
		this.conta = conta;
	}
	
	@Override
	public void run() {
		int contador = 100;
		
		for (int i = 0; i < contador; i++) {
			conta.depositar(100);
		}
	
		System.out.println("Fim dos depósitos");
	}
	
}
