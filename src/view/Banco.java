package view;

import control.Cliente;
import control.Fornecedor;
import model.ContaCorrente;

public class Banco {

	public static void main(String[] args) {
		
		ContaCorrente umaConta = new ContaCorrente();
		Fornecedor fornecedorUm = new Fornecedor(umaConta);
		Cliente clienteUm = new Cliente(umaConta);
		
		System.out.println("Saldo Inicial = " + umaConta.getSaldo());
		
		clienteUm.start();
		fornecedorUm.start();
		
		while(fornecedorUm.getState()!=Thread.State.TERMINATED || clienteUm.getState()!=Thread.State.TERMINATED){
				
		}
		
		System.out.println("Saldo Final = " + umaConta.getSaldo());
	}

}
