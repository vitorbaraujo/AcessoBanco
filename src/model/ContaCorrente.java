package model;

public class ContaCorrente {

	private float saldo = 0;
	private float temp;
	
	public float getSaldo(){
		return saldo;
	}
		
	public synchronized void depositar(float valor){
		temp = getSaldo();
		saldo = temp + valor;
	}
	
	public synchronized void sacar(float valor){
		temp = getSaldo();
		
		if(temp < valor){
			/*throwing exception of invalid arguments, such as negative values
			 * and null references 
			 */			
			throw new IllegalArgumentException();
		}
		else{
			saldo = temp - valor;
		}
	}
}
